use std::collections::HashMap;
use std::io;

fn calculate_median(numbers: &mut Vec<isize>) -> isize {
    numbers.sort();
    let median_index: usize = numbers.len() / 2;
    return numbers[median_index]
}

fn calculate_mode(numbers: &Vec<isize>) -> isize {
    let mut frequency: HashMap<isize, usize> = HashMap::new();

    for i in numbers {
        let count = frequency.entry(*i).or_insert(0);
        *count += 1;
    }

    let mut mode: isize = 0;
    let mut max_count: usize = 0;
    for (number, count) in frequency {
        if count > max_count {
            mode = number;
            max_count = count;
        }
    }
    return mode
}

fn main() {
    let mut numbers = String::new();

    io::stdin()
        .read_line(&mut numbers)
        .expect("Cannot read input");

    let mut numbers: Vec<isize> = numbers
        .split_whitespace()
        .map(|s| s.parse().expect("Parse error"))
        .collect();
    
    println!("Median: {}", calculate_median(&mut numbers));
    println!("Mode: {}", calculate_mode(&numbers));
}
