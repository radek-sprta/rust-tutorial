#[derive(Debug)]
struct Rectangle {
    height: u32,
    width: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        return self.height * self.width
    }
    
    fn can_hold(&self, other: &Rectangle) -> bool {
        return self.height > other.height && self.width > other.width
    }
}

fn main() {
    let rectangle1 = Rectangle {
        height: 50,
        width: 30,
    };
    let rectangle2 = Rectangle {
        height: 10,
        width: 20,
    };

    println!("Rectangle1 is {:?}", rectangle1);
    println!("Rectangle2 is {:?}", rectangle2);

    println!("The area of the rectangle1 is {} square pixels.", rectangle1.area());
    println!("Can rectangle1 hold rectangle2? {}", rectangle1.can_hold(&rectangle2));
}
