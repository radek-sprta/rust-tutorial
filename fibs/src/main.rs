use std::io;

fn main() {
    println!("Enter limit of how many Fibonacci numbers to print");

    let mut limit = String::new();
    io::stdin()
        .read_line(&mut limit)
        .expect("Please enter number!");

    let limit: u32 = limit.trim()
        .parse()
        .expect("Please enter number!");

    let mut a: usize = 0;
    let mut b: usize = 1;
    let mut fibs: usize;

    for i in 1..=limit {
        if i == 0 {
            println!("0");
        } else if i == 1 {
            println!("1");
        } else {
            fibs = a + b;
            println!("{fibs}");
            a = b;
            b = fibs;
        }
    }
}
